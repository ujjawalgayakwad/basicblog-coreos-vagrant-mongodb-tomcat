package basicblog

class Blog {

    String title
    String description
    
    static constraints = {
        title blank: false, nullable: false
        description blank: true, nullable: true
    }
}
