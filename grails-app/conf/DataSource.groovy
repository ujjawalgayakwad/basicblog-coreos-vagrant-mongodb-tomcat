
grails {
    mongo {
        port = 27017
        username = "demo"
        host = "172.17.8.102"
        databaseName = "blog"
        password = "password"
        options {
            autoConnectRetry = true
            connectTimeout = 3000
            connectionsPerHost = 40
            socketTimeout = 60000
            threadsAllowedToBlockForConnectionMultiplier = 5
            maxAutoConnectRetryTime=5
            maxWaitTime=120000
        }
    }
}

environments {
    development {
    }
    test {
        grails {
            mongo {
                databaseName = "blog_t"
            }
        }
    }
    production {
        grails {
            mongo {
                 port = 27017
		username = "demo"
		host = "172.17.8.102"
		databaseName = "blog"
		password = "password"
		options {
		    autoConnectRetry = true
		    connectTimeout = 3000
		    connectionsPerHost = 40
		    socketTimeout = 60000
		    threadsAllowedToBlockForConnectionMultiplier = 5
		    maxAutoConnectRetryTime=5
		    maxWaitTime=120000
		}
            }
        }
    }
}
