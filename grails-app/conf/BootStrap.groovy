import basicblog.Blog

class BootStrap {

    def init = { servletContext ->
        log.debug "Blog bootstrap started executing"
        
        Blog blogInstance
        for(i in 0..9) {
            blogInstance = new Blog()
            blogInstance.title = "Interesting Blog $i"
            blogInstance.description = "Interesting blog description."
            blogInstance.save()
        }
    }
    def destroy = {
        log.debug "Blopg bootstrap finished executing"
    }
}
