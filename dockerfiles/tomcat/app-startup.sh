#!/bin/bash

rm -rf /opt/tomcat/webapps/ROOT*
if [ -f /opt/tomcat/webapps/ROOT.war ]; then
	rm /opt/tomcat/webapps/ROOT.war
fi

cp /mnt/basicBlog/target/basicBlog*.war /opt/tomcat/webapps/ROOT.war

# This will run tomcat in foreground
/opt/tomcat/bin/catalina.sh run
